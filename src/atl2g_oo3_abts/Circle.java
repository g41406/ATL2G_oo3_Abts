/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl2g_oo3_abts;

public class Circle implements Shape {
    private Point center;
    private Double radius;

    private char color;
    
    
    public Circle(Point center,double radius){
        this.center = new Point(center.getX(),center.getY());
        this.radius = radius;
    }
    public Circle(Point center,double radius,char color){
        this(center,radius);
        this.color = color;
    }
    public Circle move(double dx, double dy) {
        center.move(dx,dy);
        return this;
    }
    @Override
    public Circle setColor(char color) {
        this.color = color;
        return this;
    }
    @Override
    public String getCmd(){
        String[] array = {
            "add",
            "circle",
            center.toString2(),
            String.valueOf(radius),
            String.valueOf(color)};

        return Main.implode(array, ' ');
    }
    public Circle move(Point move){
        center = move;
        return this;
    }
    @Override
    public Boolean isInside(Point p) {
        return (((p.getX() - center.getX()) * (p.getX() - center.getX()))
              + ((p.getY() - center.getY()) * (p.getY() - center.getY())))
                < (radius * radius);
    }
    @Override
    public char getColor() {
        return color;
    }
    @Override
    public String toString(){
        return "Cercle de centre " + center + " et de rayon " + radius;
    }
}
