/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl2g_oo3_abts;

import java.util.ArrayList;

public class Drawing {

    private ArrayList<Shape> shapes;
    private int width;
    private int height;

    public Drawing(int w, int h) {
        width = w;
        height = h;
        shapes = new ArrayList<Shape>();
    }

    public Drawing(Point siz) {
        this((int) siz.getX(), (int) siz.getY());
    }

    public void add(Shape shape) {
        shapes.add(shape);
    }

    public Shape getShape(int id) {
        return shapes.get(id);
    }

    public Shape delete(int id) {
        return shapes.remove(id);
    }

    public void printShapeList() {
        for (int i = 0; i < shapes.size(); ++i) {
            System.out.println(i + " : " + shapes.get(i));
        }
    }

    public void drawIvuritingu() {
        for (int y = 0; y <= height; ++y) {
            for (int x = 0; x <= width; ++x) {
                Boolean drawn = false;
                for (int i = 0; i < shapes.size() && !drawn; ++i) {
                    if (shapes.get(i).isInside(new Point(x, y))) {
                        System.out.print(shapes.get(i).getColor() + "" + shapes.get(i).getColor());
                        drawn = true;
                    }
                }
                if (!drawn) {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }

    public String getCmd() {
        String[] strShapes = new String[shapes.size() + 1];
        strShapes[0] = "newScene " + width + "," + height;
        for (int i = 1; i <= shapes.size(); ++i) {
            strShapes[i] = shapes.get(i - 1).getCmd();
        }
        return Main.implode(strShapes, '~');
    }
}
